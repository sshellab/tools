# Nginx reverse proxy

This tool can easy to build up reverse proxy. Support multi-container (site/api) in used in the same host environment and http/https.

## Configuration

 1. prepare SSL certificate .crt and .key named ${DOMAIN}.crt and ${DOMAIN}.key put in a folder which name certs.
   - EX: sshellab.com.crt, sshellab.com.key
 2. modify the docker-compose.yml and change the volumes to the 1. set path
 3. run `docker-compose up -d`
 4. your docker environment will create a internal network which named "nginx-reverse-proxy"
 5. attach you container to the network and set environment VIRTUAL_HOST you domain
   - run `docker run --rm -d --name sshellab -e VIRTUAL_HOST=sshellab.com --network nginx-reverse-proxy DOCKER_IMAGE`
 6. follow up 5. you can create multi site/api in the same host, however the environment is isolated by container

## Reference

 - [Multi-sites on one VPS](https://blog.ssdnodes.com/blog/host-multiple-websites-docker-nginx/)
